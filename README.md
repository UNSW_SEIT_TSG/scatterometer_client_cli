# Scatterometer Command Line Interface

Code for communicating with the [Scatterometer Server](https://gitlab.com/UNSW_SEIT_TSG/scatterometer_server) via a command line.

# Development environment configuration
The following instructions assume starting from a clean install of Windows 10 Pro.  At writing I used build version: 10.0.17134 Build 17134.
- Download and install Git for Windows from https://git-scm.com/download/win; I last used: Git-2.19.0-64-bit.exe; use VSCode as Git Editor.
- Setup dev machine for git use via ssh via https://gitlab.com/profile/keys
- Clone repo using Git for Windows by executing the command: 'git clone git@gitlab.com:UNSW_SEIT_TSG/scatterometer_gui.git'
- Download and install latest version of anaconda from https://www.anaconda.com/download/;
- Setup Anaconda environment:
    - conda env create -f environment.yml
    - conda activate scatterometer_cli
