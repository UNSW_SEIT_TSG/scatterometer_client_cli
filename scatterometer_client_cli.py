"""An application which can either send or receive data via a TCP socket."""
import socket
import sys
import argparse
def conn_sub_server(address_port, action):
    """Creates then passes a connection to the function: action."""
    try:
        s = socket.socket() # Create socket client
        s.connect(address_port) # Connect to the server
        print(f'Connected to server {address_port}')
    except socket.error as error:
        print(f'Connection failure: {error}')
        sys.exit()
    action(s)

def read(s):
    """Function for reading data from a connection: s."""
    while True:
        received_data = str(s.recv(4096), "utf-8")
        if received_data:
            print(received_data)
        if received_data == "!!!":
            break

def send(s):
    """Function for sending data via a connection: s."""
    while True:
        data_to_send = input("Send>")
        s.send(data_to_send.encode())
        if data_to_send == "!!!":
            break

MODES = ["send", "read"]
PORTS = {
    'send': 5003,
    'read': 5002
}
# IP_ADDRESS = "131.236.53.156" # Lab PC 2018 Oct 15
IP_ADDRESS = "127.0.0.1" # Local Host
ACTIONS = {
    "send": send,
    "read": read
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--mode",
        help="select either send, read",
        choices=MODES
    )
    args = parser.parse_args()

    conn_sub_server((IP_ADDRESS, PORTS[args.mode]), ACTIONS[args.mode])
